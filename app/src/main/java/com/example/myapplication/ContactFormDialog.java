package com.example.myapplication;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

public class ContactFormDialog implements DialogInterface.OnClickListener {
    Activity context;

    EditText nameView, firstnameView, emailView, mobileView;
    AlertDialog.Builder builder;
    IAddContact listener;

    public ContactFormDialog(Activity context, IAddContact listener) {
        this.context = context;
        this.listener= listener;
    }

    public void show(){
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.form_contact, null);

        nameView = (EditText) dialoglayout.findViewById(R.id.editTextNameForm);
        firstnameView = (EditText) dialoglayout.findViewById(R.id.editTextFirstnameForm);
        emailView = (EditText) dialoglayout.findViewById(R.id.editTextEmailForm);
        mobileView = (EditText) dialoglayout.findViewById(R.id.editTextMobileForm);

        builder = new AlertDialog.Builder(context);
        builder.setView(dialoglayout);
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Save", this );
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Contact contact = new Contact(  nameView.getText().toString(),
                firstnameView.getText().toString(),
                emailView.getText().toString(),
                mobileView.getText().toString());
        dialog.dismiss();
        listener.onAddContact(contact);
    }
}
