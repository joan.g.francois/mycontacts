package com.example.myapplication;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Helper {

    public static String dataToJson(ArrayList<Contact> contacts){
        List<Contact> contactList = new ArrayList<Contact>(contacts.size());
        for (Contact c: contacts) {
            contactList.add(c);
        }
        Gson gson = new Gson();
        String jsonString = gson.toJson(contactList);
        return  jsonString;
    }

    public static ArrayList<Contact> jsonToData(String json){
        Gson gson = new Gson();
        Type typeMyType = new TypeToken<ArrayList<Contact>>(){}.getType();
        ArrayList<Contact> contacts = gson.fromJson(json, typeMyType);
        return contacts;
    }

    public static ArrayList<Contact> generateData(){
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        contacts.add( new Contact("François", "Joan", "j.francois@cfa-insta.fr", "0633763367"));
        contacts.add( new Contact("Kilani", "Imad", "i.kilani@cfa-insta.fr", "0143367556"));
        contacts.add( new Contact("Chenib", "Ali", "a.chenib@cfa-insta.fr", "0143367556"));
        contacts.add( new Contact("Coste", "Mounira", "m.coste@cfa-insta.fr", "0143367556"));
        contacts.add( new Contact("Sert", "Selin", "s.sert@cfa-insta.fr", "0143367556"));
        return contacts;
    }
}
