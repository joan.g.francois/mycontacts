package com.example.myapplication;

import java.io.Serializable;

public class Contact implements Serializable {

    private String name;
    private String firstname;
    private String email;
    private String mobile;

    public Contact(String name, String firstname, String email, String mobile) {
        this.name = name;
        this.firstname = firstname;
        this.email = email;
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
