package com.example.myapplication;

public interface IAddContact {

    public void onAddContact(Contact c);
}
