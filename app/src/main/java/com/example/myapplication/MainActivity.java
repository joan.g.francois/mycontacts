package com.example.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public class MainActivity  extends AppCompatActivity implements View.OnClickListener, IAddContact {

    ListView contactListView;
    ContactAdapter adapter;
    Button buttonAddContact;
    ArrayList<Contact> contacts;
    ContactFormDialog formDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        formDialog= new ContactFormDialog(this, this);

        SharedPreferences settings = getSharedPreferences("contactsApp", 0);
        String jsonData= settings.getString("contacts", "");

        if(jsonData.isEmpty()){
            contacts = Helper.generateData();//new ArrayList<Contact>();//Helper.generateData();
        }else{
            contacts = Helper.jsonToData(jsonData);
        }
        contactListView = (ListView) findViewById(R.id.listViewContacts);
        adapter= new ContactAdapter(contacts, this);
        contactListView.setAdapter(adapter);
        registerForContextMenu(contactListView);

        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra("contact", contacts.get(position));
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });

        buttonAddContact = (Button) findViewById(R.id.buttonAddContact);
        buttonAddContact.setOnClickListener(this);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Contact contact = (Contact) getIntent().getSerializableExtra("updateContact");
        int currentPosition= getIntent().getIntExtra("position", -1);
        if(currentPosition >= 0){
            contacts.set(currentPosition, contact);
            saveContacts();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Contact contact= contacts.get(menuInfo.position);
        switch (item.getItemId()){
            case R.id.itemButtonCall:
                Intent intentCall= new Intent(Intent.ACTION_CALL);
                intentCall.setData(Uri.parse("tel:"+contact.getMobile()));
                int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 123);
                } else {
                    startActivity(intentCall);
                }
                break;
            case R.id.itemButtonSendMail:
                Intent intentMail = new Intent(Intent.ACTION_SENDTO);
                intentMail.setData(Uri.parse("mailto:"+contact.getEmail()));
                startActivity(intentMail);
                break;
            default:
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra("contact", contact);
                intent.putExtra("position", menuInfo.position);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        formDialog.show();
    }

    @Override
    public void onAddContact(Contact c) {
        contacts.add(c);
        saveContacts();
    }

    public void saveContacts(){
        adapter.notifyDataSetChanged();
        SharedPreferences settings = getSharedPreferences("contactsApp", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("contacts", Helper.dataToJson(contacts));
        editor.commit();
    }
}
