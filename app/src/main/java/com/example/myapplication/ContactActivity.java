package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ContactActivity extends AppCompatActivity implements View.OnClickListener {

    EditText nameView, firstnameView, emailView, mobileView;
    Button buttonCancel, buttonSave;
    int currentPosition= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        nameView = (EditText) findViewById(R.id.editTextName);
        firstnameView = (EditText) findViewById(R.id.editTextFirstname);
        emailView = (EditText) findViewById(R.id.editTextEmail);
        mobileView = (EditText) findViewById(R.id.editTextMobile);

        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setVisibility(View.INVISIBLE);
        buttonCancel.setOnClickListener(this);

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        setContactToView((Contact) intent.getSerializableExtra("contact"));
        currentPosition= intent.getIntExtra("position", 0);
    }

    public void setContactToView(Contact contact){
        nameView.setText(contact.getName());
        firstnameView.setText(contact.getFirstname());
        emailView.setText(contact.getEmail());
        mobileView.setText(contact.getMobile());
    }

    @Override
    public void onClick(View v) {
        Button button= (Button) v;
        if(button.equals(buttonCancel)){
            buttonCancel.setVisibility(View.INVISIBLE);
            buttonSave.setText(R.string.button_edit);
            nameView.setEnabled(false);
            firstnameView.setEnabled(false);
            emailView.setEnabled(false);
            mobileView.setEnabled(false);
        }else{
            //Save action
            if(buttonSave.getText().toString().equals( getResources().getString(R.string.button_save))){
                // 1) recceuiller les datas des views et créer objet Contact
                Contact contact= new Contact(nameView.getText().toString(),
                                            firstnameView.getText().toString(),
                                            emailView.getText().toString(),
                                            mobileView.getText().toString());
                // 2) envoyer un intent avec les données du contacts pour le modifier
                Intent intent = new Intent(ContactActivity.this, MainActivity.class);
                intent.putExtra("updateContact", contact);
                intent.putExtra("position", currentPosition);
                startActivity(intent);
                // 3) enregistrer les données et mettre à jours la liste view du main
            }else{ // Edit Action
                buttonCancel.setVisibility(View.VISIBLE);
                buttonSave.setText(R.string.button_save);
                nameView.setEnabled(true);
                firstnameView.setEnabled(true);
                emailView.setEnabled(true);
                mobileView.setEnabled(true);
            }


        }
    }
}